# README #

This is my first attempt at a C# and RabbitMQ .NET Core console application. I purchased a udemy course called "Getting Started with .NET Core Microservices RabbitMQ" by Manish Narayan.

### Installation ###

* Extract the zip file.
* Click on the Solution file and open.
* Version 1.0


### How do I get set up? ###

* Microsoft Visual Studio 2019
* Configuration
* Dependencies: RabbitMQ server software. RabbitMQ.Client nuGet package. Install current version of erlang (for RabbitMQ)
* Run without debugging will add messages that can be viewed on the RabbitMQ admin console.
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact